import spock.lang.Specification

//<editor-fold desc="No Sneak Peek">
class LibraryTest extends Specification{

    Library lib = new Library()

    def "test reading kommuner returns 99 of them"() {
        when:
        def result = lib.municipals
        then:
        result.size() == 99
    }
}
//</editor-fold>
