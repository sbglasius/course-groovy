// In this exercise we will chain calls to collection methods

def municipals = new Library().municipals

// Make a list of county areas, where the county name is the key and the value is the area,
// in decending order
def countyAreas = municipals.groupBy { it.county }.collectEntries { key, value -> [key, value.sum { it.area }]}.sort { -it.value }
println "-- County areas --"
println countyAreas

// Find the largest county and print the name
def largestCounty = municipals.groupBy { it.county }.collectEntries { key, value -> [key, value.sum { it.area }]}.max { it.value }.key
println "-- Largest County  --"
println largestCounty



