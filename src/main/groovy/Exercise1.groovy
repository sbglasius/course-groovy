// For simmplicity we have one library that contains the Danish municipals
// The data comes from http://geo.oiorest.dk/kommuner.json but is mangled by the Library
// class.
def municipals = new Library().municipals

// For documentation go to http://groovy-lang.org/gdk.html

// Prints the content of the library's municipals (to see the structure)
Library.prettyPrint(municipals)


// Print the size of the municipals list
println "-- Number of municipals --"
println municipals.size()

// Create a list of municipal names and print it
def municipalNames = municipals.name // or municipals*.name or municipals.collect { it.name }
println "-- Municipal Names --"
println municipalNames

// Create a list of county names and print it (will contain duplicates)
def countyNames = municipals.county // or municipals*.county or municipals.collect { it.region }
println "-- County Names --"
println countyNames

// The list of county names with unique names
def uniqueCountyNames = municipals.county.unique()  // or countyNames.unique()
println "-- Unique County Names --"
println uniqueCountyNames

// Sort municipals by area
def municipalsByArea = municipals.sort { it.area }
println "-- Municipals sorted by area --"
Library.prettyPrint municipalsByArea

// Sort municipals by region and then by area
def municipalsByRegionAndArea = municipals.sort { a,b -> a.region <=> b.region ?: a.area <=> b.area }
println "-- Municipals sorted by region and then area --"
Library.prettyPrint municipalsByRegionAndArea

// Group the list by county
def municipalsGroupedByRegion = municipals.groupBy { it.region }
println "-- Municipals grouped by region --"
println municipalsGroupedByRegion

// Find largest and smallest municipal
def largestMunicpal = municipals.min { it.area }
def smallestMunicipal = municipals.max { it.area }
println "-- Largest and smallest municipal --"
println largestMunicpal
println smallestMunicipal

// Find municipals over 500km2
def municipalsOver500 = municipals.findAll { it.area >= 500 }
println "-- Municipals over 500km2 --"
println municipalsOver500

// Is every municipal under 1000km2
def isEveryMunicipalUnder1000 = municipals.every { it.area <= 1000}
println "-- Is Every Municipals under 1000km2 --"
println isEveryMunicipalUnder1000

// Does any of the municipal names begin with the letter 'I'
def isAnyMunicipalNameBeginingWithI = municipals.any { it.name.startsWith('I') }
println "-- Is Any Municipal starting with the letter 'I' --"
println isAnyMunicipalNameBeginingWithI



