import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil

// Make a markup builder that will produce output in this format:
// Note: The content is the same, but now we have an XML pre-ample and a namespace

/*
<?xml version="1.0" encoding="UTF-8"?>
<counties xmlns:meta="http://meta/attributes">
   <county name="" area="">
        <municipals>
            <municipal number="">
                <meta:name>name</name>
                <meta:area>area</area>
            </municipal>
            </municipal>...</municipal>
        </municipal>
   </county>
   <county>...</county>
</counties>
*/

// This code provides a startingpoint
def groupedByCounty = new Library().municipals.groupBy { it.county }

def builder = new StreamingMarkupBuilder()
builder.encoding = 'UTF-8'

def counties = builder.bind {
    mkp.xmlDeclaration()
    namespaces << [meta:'http://meta/attributes']
    counties {
        groupedByCounty.each { countyName, municipalList ->
            def countyArea = municipalList.area.sum()
            county(name: countyName, area: countyArea) {
                municipals {
                    municipalList.each { municipalEntry ->
                        municipal(number: municipalEntry.number) {
                            'meta:name'(municipalEntry.name)
                            'meta:area'(municipalEntry.area)
                        }
                    }
                }
            }
        }
    }
}

println XmlUtil.serialize(counties)