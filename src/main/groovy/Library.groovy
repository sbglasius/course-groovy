import groovy.json.JsonSlurper

//<editor-fold desc="No Sneak Peek">
class Library {
    private List<Map<String,String>> rawData
    Library() {
        def inputStream = this.getClass().getResourceAsStream '/kommuner.json'
        rawData = new JsonSlurper().parse(inputStream)
    }

    List<Map<String,Object>> getMunicipals() {
        rawData.collect {
            [
                    number: it.nr,
                    name: it.navn,
                    area: (it.areal as Long) / (1000*1000),
                    county: it.region.navn,
            ]
        }
    }
    static void prettyPrint(List<Map<String,String>> municipals) {
        println "["
        municipals.each {
            println "   $it,"
        }
        println "]"

    }

    static String getMunicipalXml() {
        this.getClass().getResource('/kommuner.xml').text
    }

    static String getMunicipalJson() {
        this.getClass().getResource('/kommuner.json').text
    }
}
//</editor-fold>
