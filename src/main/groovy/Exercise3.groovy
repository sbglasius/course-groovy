import groovy.xml.MarkupBuilder

// Make a markup builder that will produce output in this format:

/*
<counties>
   <county name="" area="">
        <municipals>
            <municipal number="">
                <name>name</name>
                <area>area</area>
            </municipal>
            </municipal>...</municipal>
        </municipal>
   </county>
   <county>...</county>
</counties>
*/

// This code provides a startingpoint
def groupedByCounty = new Library().municipals.groupBy { it.county }

def xml = new MarkupBuilder()
xml.doubleQuotes = true

xml.counties {
    groupedByCounty.each { countyName, municipalList ->
        def countyArea = municipalList.area.sum()
        county(name: countyName, area: countyArea) {
            municipals {
                municipalList.each { municipalEntry ->
                    municipal(number: municipalEntry.number) {
                        name(municipalEntry.name)
                        area(municipalEntry.area)
                    }
                }
            }
        }
    }
}

println xml.toString()