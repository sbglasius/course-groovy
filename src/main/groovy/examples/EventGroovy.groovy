package examples

class EventGroovy implements List<Speaker> {
    @Delegate List<Speaker> speakersDelagate = []

    @Override
    boolean add(Speaker speaker) { speakersDelagate.add(validatedSpeaker(speaker)) }

    @Override
    boolean addAll(Collection<? extends Speaker> c) { speakersDelagate.addAll(validatedSpeakers(c)) }

    @Override
    boolean addAll(int index, Collection<? extends Speaker> c) { speakersDelagate.addAll(index, validatedSpeakers(c)) }

    @Override
    Speaker set(int index, Speaker element) { speakersDelagate.set(index, validatedSpeaker(element)) }

    @Override
    void add(int index, Speaker element) { speakersDelagate.add(index, validatedSpeaker(element)) }

    private static Speaker validatedSpeaker(Speaker speaker) {
        if (!speaker.name) throw new IllegalArgumentException("Speaker name can not be null or empty")
        return speaker
    }

    private static Collection<? extends Speaker> validatedSpeakers(Collection<? extends Speaker> speakers) {
        speakers.every { validatedSpeaker(it)}
        return speakers
    }

}
