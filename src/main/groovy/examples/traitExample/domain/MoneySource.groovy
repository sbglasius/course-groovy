package examples.traitExample.domain

interface MoneySource {
    void withdraw(double amount, MoneyDestination destination)
}

trait TransferMoneySource implements MoneySource {

    void withdraw(double amount, MoneyDestination destination) {
        def currentBalance = this.balance
        if (currentBalance > amount) {
            this.decreaseBalance(amount)
            destination.deposit(amount)
            this.updateLog "Withdrawal of ${amount} performed"
        } else {
            throw new IllegalArgumentException("Insufficient Balance in Source")
        }
    }

}