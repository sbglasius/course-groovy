package examples.traitExample.domain

trait Account {
    double balance = 0

    void increaseBalance(double amount) {
        balance = balance + amount
    }

    void decreaseBalance(double amount) {
        balance = balance - amount
    }

    void updateLog(msg) {
        println "${getClass().simpleName}: $msg"
    }

    String toString() {
        "${getClass().simpleName} balance: $balance"
    }
}

class SavingsAccount implements Account {
    SavingsAccount(double balance) {
        this.increaseBalance(balance)
    }
}

class CheckingAccount implements Account {
    CheckingAccount(double balance) {
        this.increaseBalance(balance)
    }
}