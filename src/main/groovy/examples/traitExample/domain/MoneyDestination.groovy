package examples.traitExample.domain

interface MoneyDestination {
    void deposit(double amount)
}

trait TransferMoneyDestination implements MoneyDestination {
    public void deposit(double amount) {
        this.updateLog "Deposit of ${amount} performed"
        increaseBalance(amount)
    }
}