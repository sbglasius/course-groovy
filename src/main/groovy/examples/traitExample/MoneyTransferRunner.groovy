package examples.traitExample

import examples.traitExample.domain.CheckingAccount
import examples.traitExample.domain.SavingsAccount
import examples.traitExample.usecase.MoneyTransferContext

def savingsAccount = new SavingsAccount(200)

def checkingsAccount = new CheckingAccount(100)

println "$savingsAccount, $checkingsAccount"

MoneyTransferContext.transfer(100).from(savingsAccount).to(checkingsAccount)

println "$savingsAccount, $checkingsAccount"
