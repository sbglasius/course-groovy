package examples.traitExample.usecase

import examples.traitExample.domain.Account
import examples.traitExample.domain.MoneyDestination
import examples.traitExample.domain.MoneySource
import examples.traitExample.domain.TransferMoneyDestination
import examples.traitExample.domain.TransferMoneySource

class MoneyTransferContext {
    private Account source
    private double amount

    static MoneyTransferContext transfer(double amount) {
        return new MoneyTransferContext(amount: amount)
    }

    MoneyTransferContext from(Account source) {
        this.source = source
        return this
    }


    void to(Account destination) {
        // Apply the role of a MoneySource to a source Account
        MoneySource moneySource = source as TransferMoneySource
        // Apply the role of a MoneyDestination to a destination Account
        MoneyDestination moneyDestination = destination as TransferMoneyDestination
        // Perform the usecase
        moneySource.withdraw(amount, moneyDestination)
    }

}
