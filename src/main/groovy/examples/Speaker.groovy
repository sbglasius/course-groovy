package examples

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.transform.TypeChecked

@TypeChecked
@EqualsAndHashCode
@ToString(includeNames = true)
class Speaker {
    String name
    String phoneNumber
    String emailAddress

    boolean hasEmailAddres() {
        emailAddress != null
    }
}
