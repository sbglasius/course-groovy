package examples

class SpeakerJavaish {
    String name
    String phoneNumber
    String emailAddress

    String toString() {
        "${getClass().simpleName}(name=${name}, phoneNumber=${phoneNumber}, emailAddress=${emailAddress}))"
    }

    public boolean equals(Object other) {
        if (other == null) return false
        if (this.is(other)) return true
        if (SpeakerJavaish != other.getClass()) return false
        if (name != other.name) return false
        if (phoneNumber != other.phoneNumber) return false
        if (emailAddress != other.emailAddress) return false
        return true
    }

    @Override
    int hashCode() {
        Objects.hash(name, phoneNumber, emailAddress)
    }
}