package examples

import groovy.transform.CompileStatic

@CompileStatic
class EventJavaish implements List<Speaker> {
    List<Speaker> speakersDelagate = []

    @Override
    int size() { speakersDelagate.size() }

    @Override
    boolean isEmpty() { speakersDelagate.empty }

    @Override
    boolean contains(Object o) { speakersDelagate.contains(o) }

    @Override
    Iterator<Speaker> iterator() { speakersDelagate.iterator() }

    @Override
    Object[] toArray() { speakersDelagate.toArray() }

    @Override
    def <T> T[] toArray(T[] a) { speakersDelagate.toArray(a) }

    @Override
    boolean add(Speaker speaker) { speakersDelagate.add(validatedSpeaker(speaker)) }

    @Override
    boolean remove(Object o) { speakersDelagate.remove(o) }

    @Override
    boolean containsAll(Collection<?> c) { speakersDelagate.containsAll(c) }

    @Override
    boolean addAll(Collection<? extends Speaker> c) { speakersDelagate.addAll(validatedSpeakers(c)) }

    @Override
    boolean addAll(int index, Collection<? extends Speaker> c) { speakersDelagate.addAll(index, validatedSpeakers(c)) }

    @Override
    boolean removeAll(Collection<?> c) { speakersDelagate.removeAll() }

    @Override
    boolean retainAll(Collection<?> c) { speakersDelagate.retainAll(c) }

    @Override
    void clear() { speakersDelagate.clear() }

    @Override
    Speaker get(int index) { speakersDelagate.get(index) }

    @Override
    Speaker set(int index, Speaker element) { speakersDelagate.set(index, validatedSpeaker(element)) }


    @Override
    void add(int index, Speaker element) { speakersDelagate.add(index, validatedSpeaker(element)) }

    @Override
    Speaker remove(int index) { speakersDelagate.remove(index) }

    @Override
    int indexOf(Object o) { speakersDelagate.indexOf(o) }

    @Override
    int lastIndexOf(Object o) { speakersDelagate.lastIndexOf(o) }

    @Override
    ListIterator<Speaker> listIterator() { speakersDelagate.listIterator() }

    @Override
    ListIterator<Speaker> listIterator(int index) { speakersDelagate.listIterator(index) }

    @Override
    List<Speaker> subList(int fromIndex, int toIndex) { speakersDelagate.subList(fromIndex, toIndex) }

    private static Speaker validatedSpeaker(Speaker speaker) {
        if (!speaker.name) throw new IllegalArgumentException("Speaker name can not be null or empty")
        return speaker
    }

    private static Collection<? extends Speaker> validatedSpeakers(Collection<? extends Speaker> speakers) {
        speakers.every { validatedSpeaker(it)}
        return speakers
    }

}
