// Read and print municipal names and their county href. Data is ready by the Library.municipalXml

def xmlText = Library.municipalXml

def xmlParser = new XmlParser().parseText(xmlText)

xmlParser.kommune.each {
    println "${it.navn.text()} ${it.region.'@href'[0]}"
}

println ("-"*80)

def xmlSlurper = new XmlSlurper().parseText(xmlText)

xmlSlurper.kommune.each {
    println "${it.navn.text()} ${it.region.'@href'.text()}"
}
