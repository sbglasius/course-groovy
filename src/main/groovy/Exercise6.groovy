import groovy.json.JsonBuilder

// In this exercise we want to output JSON in the same way we did XML in Exercise 3

// This code provides a startingpoint
def groupedByCounty = new Library().municipals.groupBy { it.county }

/*
[
    {
        "name": "",
        "municipals": [
            {
                "number": "",
                "name": "",
                "area": 0,
                "county": ""
            },
            {
                ... Next municipal
            },
        ]
    },  {
            ... Next county
        }
    }
]
 */

def builder = new JsonBuilder()


builder(
        groupedByCounty.collect { countyName, municipalEntries ->
            [
                    name      : countyName,
                    municipals: municipalEntries
            ]
        }
)

println builder.toPrettyString()

println("-" * 80)
// Alternative solution:

def builder2 = new JsonBuilder(groupedByCounty.collect {
    [name: it.key, municipals: it.value]
})
println builder2.toPrettyString()