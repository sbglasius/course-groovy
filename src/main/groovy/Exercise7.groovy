import groovy.json.JsonSlurper

// Read and print municipal names and their county href. Data is ready by the Library.municipalJson

def jsonText = Library.municipalJson

def json = new JsonSlurper().parseText(jsonText)

json.each {
    println "${it.navn} ${it.region.href}"
}
